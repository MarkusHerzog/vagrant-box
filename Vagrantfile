# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.require_version ">= 1.3.5"
unless Vagrant.has_plugin?("vagrant-librarian-puppet")
  raise 'please install vagrant-librarian-puppet by running: vagrant plugin install vagrant-librarian-puppet'
end

Vagrant.configure("2") do |config|
  config.vm.box = "puppetlabs/ubuntu-14.04-64-puppet"

  config.vm.hostname = "box.dev"

  config.vm.network :private_network, ip: "10.11.12.104"

  config.ssh.forward_agent = true

  config.vm.boot_timeout = 900

  # Configure your project folders here
  config.vm.synced_folder "../amazers/subarashi", "/home/vagrant/subarashi", type: "nfs", mount_options: ['rw', 'vers=3', 'tcp', 'fsc']  # the fsc is for cachedfilesd
  config.vm.synced_folder "../amazers/Amazers", "/home/vagrant/amazers", type: "nfs", mount_options: ['rw', 'vers=3', 'tcp', 'fsc']  # the fsc is for cachedfilesd
  config.vm.synced_folder "../amazers/Brochure", "/home/vagrant/brochure", type: "nfs", mount_options: ['rw', 'vers=3', 'tcp', 'fsc']  # the fsc is for cachedfilesd

  config.vm.provider :virtualbox do |v|
    v.name = "subarashi.dev"
    v.cpus = 1
    v.memory = 2048
  end

  if Vagrant.has_plugin?("vagrant-dns")
    config.dns.tld = "dev"
    config.dns.patterns = [/^*\.subarashi\.dev$/]
  end

  config.vm.provision :file, source: '~/.gitconfig', destination: '/home/vagrant/.gitconfig' if File.exist?(ENV['HOME'] + '/.gitconfig')
  config.vm.provision :file, source: '~/.gitignore_global', destination: '/home/vagrant/.gitignore_global' if File.exist?(ENV['HOME'] + '/.gitignore_global')

  config.vm.provision :puppet do |puppet|
     puppet.manifests_path = "manifests"
     puppet.manifest_file  = "manifest.pp"
     puppet.module_path = "modules"
  end
end
