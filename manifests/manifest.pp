exec { 'apt-get update':
  command => "/usr/bin/apt-get update",
}
Exec["apt-get update"] -> Package <| |>

package { ['git', 'language-pack-de', 'python-software-properties', 'vim', 'build-essential', 'zlib1g-dev', 'libssl-dev', 'libffi-dev', 'libreadline6-dev', 'libyaml-dev', 'libmysqlclient-dev', 'sqlite3', 'libsqlite3-dev', 'phantomjs', 'cachefilesd', 'imagemagick']:
  ensure => present,
}

exec { "enable cachefilesd" :
    command => "echo 'RUN=yes' > /etc/default/cachefilesd",
    path => "/usr/bin:/bin",
    require => Package["cachefilesd"],
    notify => Service["cachefilesd"],
}->
service { "cachefilesd":
    ensure  => "running",
    enable  => "true",
}

class { 'apt': }

apt::source { 'postgresql':
  location          => 'http://apt.postgresql.org/pub/repos/apt/',
  release           => 'precise-pgdg',
  repos             => 'main',
  required_packages => 'debian-keyring debian-archive-keyring',
  key               => 'ACCC4CF8',
  key_source        => 'http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc',
}->
package { ['postgresql-common', 'postgresql-9.4', 'postgresql-client-9.4', 'postgresql-server-dev-9.4']:
  ensure => present,
}->
exec {'update-rc.d postgresql defaults':
  path => '/usr/sbin',
}

rbenv::install { "vagrant":
}->
rbenv::compile { "2.2.0":
  user => "vagrant",
  global => true,
}->
rbenv::compile { "2.1.2":
  user => "vagrant",
  global => false,
}->
rbenv::gem { "bundle for ruby 2.1.2":
  gem => "bundle",
  user => "vagrant",
  ruby => "2.1.2",
} 
apt::ppa { "ppa:chris-lea/node.js":
}->
package { 'nodejs':
  ensure   => present,
}

exec { "heroku" :
    command => "wget -q https://toolbelt.heroku.com/install.sh -O /tmp/install.sh && sh /tmp/install.sh && echo 'PATH=\"/usr/local/heroku/bin:\$PATH\"' >> /home/vagrant/.profile",
    path => "/usr/bin:/bin",
    creates => "/tmp/install.sh",
}

# mysql
class { 'mysql::server':
  root_password => 'vagrant'
}

# mongodb
apt::source { 'mongodb':
  location   => 'http://downloads-distro.mongodb.org/repo/ubuntu-upstart',
  release    => 'dist',
  repos      => '10gen',
  key        => '7F0CEB10',
  key_server => 'keyserver.ubuntu.com',
  include_src => false,
}->
package { 'mongodb-10gen':
  ensure => present,
}

# java
apt::ppa { "ppa:webupd8team/java":
}->
exec {
  'set-licence-selected':
    command => '/bin/echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections';
  'set-licence-seen':
    command => '/bin/echo debconf shared/accepted-oracle-license-v1-1 seen true | /usr/bin/debconf-set-selections';
}->
package { 'oracle-java8-installer':
  ensure   => present,
}

